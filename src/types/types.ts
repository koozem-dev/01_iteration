export interface Item {
  id: number,
  name: string,
  date: string,
  genre: string,
  author: string,
  value: number,
}

export type IData = {
  name: string,
  web_url: string,
  id: string
}

export interface ISubs {
  number: string,
  cost: number,
  id: number
}

export interface IState {
  subs: ISubs[],
}

export interface IStateNearly {
  nearly: Item[] | [],
  loading: boolean,
  error: null | {}
}