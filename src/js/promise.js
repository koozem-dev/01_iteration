function noop() {}

class MyPromise {
  constructor(executor) {
    this.queue = [];
    this.errorHandler = noop;
    this.finallyHandler = noop;

    try {
      executor.call(null, this.onResolve.bind(this), this.onReject.bind(this));
    } catch (e) {
      this.errorHandler(e);
    } finally {
      this.finallyHandler();
    }
  }

  onResolve(data) {
    this.queue.forEach(callback => {
      data = callback(data);
    });

    this.finallyHandler();
  }

  onReject(error) {
    this.errorHandler(error);
    this.finallyHandler();
  }

  then(fn) {
    this.queue.push(fn);
    return this;
  }

  catch(fn) {
    this.errorHandler = fn;
    return this;
  }

  finally(fn) {
    this.finallyHandler = fn;
    return this;
  }
}

// const promise = new Promise((resolve, reject) => {
const promise = new MyPromise((resolve, reject) => {
  setTimeout(() => {
    // resolve('hello from promise');
    reject('some error promise');
  }, 150);
});

promise.then(result => result.toUpperCase())
  .then(res => console.log('myPromise result:', res))
  .catch(err => console.log('error:', err))
  .finally(() => console.log('just finally'));