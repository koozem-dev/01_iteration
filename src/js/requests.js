
const groupId = 3849457;
let element = document.createElement('div');
const url = `https://gitlab.com/api/v4/groups/${groupId}/projects?simple=true`;

document.addEventListener('DOMContentLoaded', () => {
  const rand = Math.floor(Math.random() * 3);
  console.log(rand);
  if (rand === 1) {
    getDataWithFetch(url);
  }
  else if (rand === 2) {
     getDataWithAsync(url);
  } else {
    getDataWithXHR(url);
  }
});

const getDataWithFetch = (url) => {
  console.log('with fetch');
  fetch(url)
    .then(response => response.json())
    .then(data => {
      createBlock(data, true);
    })
    .catch(error => {
      createBlock(error, false);
    })
};

const getContent = async (url) => {
  console.log('with async');
  const result = await fetch(url);

  if (!result.ok) {
    createBlock(result.status, false);
    throw new Error('just error');
  }
  return result.json();
};

const getDataWithAsync = async (url) => {
  const res = await getContent(url);
  createBlock(res, true);
}

const getDataWithXHR = (url) => {
  console.log('with XHR');
  let xhr = new XMLHttpRequest();

  xhr.open('GET', url);
  xhr.responseType = 'json';
  xhr.send();

  xhr.onload = () => {
    const data = xhr.response;
    createBlock(data, true);
  };

  xhr.onerror = error => {
    createBlock(error, false);
  };
};

const createBlock = (data, loaded) => {
  createItems(data, loaded);
  addItems();
}

const createItems = (arr = [], loaded) => {
  if (loaded) {
    element.innerHTML = arr.reduce((content, item) => {
      const { name, web_url, id } = item;
      content += `<div class="git-repo-block"><span>name: ${name}</span> <span>web_url: ${web_url}</span> <span>id: ${id}</span></div>`;
      return content;
    }, '');
  } else {
    element.innerHTML = `<div class="git-repo-block"><span>При загрузке произошла ошибка: <br> ${arr}</span></div>`;
  }
  return element;
};

const addItems = () => {
  element.classList.add('git-repo');
  element.classList.add('container');
  element.classList.add('container-footer');
  document.body.append(element);
};