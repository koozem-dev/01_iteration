
// let tickets = 0;
//
// document.addEventListener('click', function(event) {
//
//   if (~event.target.className.indexOf('nearly-buy')) {
//     tickets++;
//     console.log('Количество купленных билетов:' + tickets);
//   }
// });

document.addEventListener('DOMContentLoaded', () => {

  const formText = document.querySelector('.form-text');

  const form = document.querySelector('.form');
  form.addEventListener('submit', () => {

    let message = document.createElement('div');
    const submit = document.querySelector('.btn-submit');
    message.innerText = 'Данные успешно отправлены';

    message.className = 'message';
    submit.append(message);

    setTimeout(() => {
      message.remove();
    }, 3000);
  });

  const inputs = document.getElementsByTagName('input');
  for (const inputElement of inputs) {
    inputElement.onfocus = function(e) {
      console.log(e.target.name);
    };
  }

  // function* generateSequence() {
  //   yield 1;
  //   yield 'js';
  //   yield false;
  // }

  // let someProps = [...generateSequence()];

  // const subscriptions = new SubWithDiscount(5, ...someProps,
  //   true).getAllCosts(); // just for Spread demo
  // tableCost.innerHTML += subscriptionsList(subscriptions);
});

class Subscription {

  subs = [
    { number: '4 сеанса', cost: 2050 },
    { number: '6 сеансов', cost: 3100 },
    { number: '8 сеансов', cost: 4100 },
  ];

  constructor(...rest) { // just for Rest demo
    let [someNumber, someString, someBoolean] = rest;
    console.log(someNumber, someString, someBoolean);
  }

  getAllCosts = () => {
    return this.subs;
  };
}

class SubWithDiscount extends Subscription {

  constructor(discount, ...rest) { // just for Rest demo
    super(...rest);
    this.discount = discount;
  }

  applyDiscount = () => {
    return this.subs.map((value) => {
      value.cost = Math.floor(value.cost * (100 - this.discount) / 100);
      return value;
    });
  };

  getAllCosts = () => {
    return this.applyDiscount();
  };
}
