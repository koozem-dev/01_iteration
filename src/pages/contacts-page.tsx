import React, {Component} from 'react';
import Form from '../components/form';
import ErrorBoundary from '../components/error-boundary';

export default class ContactsPage extends Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {counter: 0};
  }

  componentDidMount() {
    console.log('ContactsPage: я в DOMе');
  }

  componentDidUpdate() {
    console.log('ContactsPage: в DOMе ремонт', this.state.counter);
  }

  componentWillUnmount() {
    console.log('ContactsPage: i`ll be back baby');
  }

  render() {
    console.log('ContactsPage: я родился');
    return (
      <main>
        <div className="container container-contact-us">
          <section className="contact-us">
            <h1 className="contact-us-header">Напишите нам</h1>
            <ErrorBoundary>
              <Form />
            </ErrorBoundary>
          </section>
        </div>
      </main>
    );
  }
}