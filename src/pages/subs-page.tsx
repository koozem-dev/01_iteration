import React from 'react';
import {
  MyChartLine,
  MyChartBar,
  MyChartPie,
  JSChartBar,
} from '../components/charts';

const SubsPage: React.FC = () => {

  return (
    <main>
      <div className="container container-main">
        <MyChartLine />
        <MyChartBar />
        <MyChartPie />
        <JSChartBar />
      </div>
    </main>
  );
};

export default SubsPage;