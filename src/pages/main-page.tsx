import React, {useState} from 'react';
import NearlyList from '../components/nearly-list';
import SubscriptionsList from '../components/subscriptions-list';
import {IState} from '../types/types';
import styles from './main-page.module.css';
import UiButton from "../components/Ui/UiButton/UiButton";

type MainPageProps = {
  prop: IState
}

const MainPage: React.FC<MainPageProps> = ({prop}) => {
  const {subs} = prop;
  const [sale, setSale] = useState<number>(0);
  const [classes, setClasses] = useState<'btnSubmit' | 'buy' | 'displayNone' | 'btn'>('displayNone');

  const addSale = () => {
    setSale(Math.floor(Math.random() * 10 + 1));
    setClasses('btn');
  }

  return (
    <main>
      <div className={styles.containerMain}>
        <NearlyList></NearlyList>
      </div>
      <div className={styles.containerSubscription}>
        <section className={styles.subscription}>
          <h1 className={styles.header}>Абонементы в Камерный</h1>
          <div className={styles.content}>
            <div className={styles.info}>
              <table className={styles.cost} id="subscription-cost">
                <caption>Стоимость</caption>
                <thead>
                <tr>
                  <th scope="col">Количество</th>
                  <th scope="col">Цена</th>
                </tr>
                </thead>
                <tbody><SubscriptionsList subs={subs} /></tbody>
                <tfoot>{/**/}</tfoot>
              </table>
            </div>
            <div className={styles.info}>
              <p className={styles.infoText}>Уважаемые зрители!<br />
                В кассе театра, вы можете приобрести абонементы на наши
                спектакли. <br />
                Узнать подробнее вы можете по телефонам:</p>
              <p>
                <a href="tel:+79101500941">+7(910) 150-09-41</a>, <a
                href="tel:84872304596">8(4872) 30-45-96</a>
              </p>
            </div>
            <div className={styles.info}>
              <UiButton onClick={addSale}>Хочу скидку</UiButton>
              <p>Вы выиграли скидку <span>{sale}%</span></p>
              <UiButton classes={classes} id="addSale">Применить к абонементу</UiButton>
            </div>
          </div>

        </section>
      </div>
    </main>
  );
};

export default MainPage;