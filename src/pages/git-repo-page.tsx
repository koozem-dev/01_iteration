import React, {PureComponent} from 'react';

import GitRepo from "../components/git-repo/git-repo";
import styles from './main-page.module.css';
import JustContext from "../context/just-context";

const GitRepoPage: React.FC = () => {
  return (
    <main>
      <div className={styles.containerMain}>
        <GitRepoWithContext />
      </div>
    </main>
  );
};

class GitRepoWithContext extends PureComponent<any, any> {
  toggleMessage = (newMessage: string): void => {
    this.setState({message: newMessage});
  };
  // Состояние хранит функцию для обновления контекста,
  // которая будет также передана в Provider-компонент.
  state = {
    message: 'hello',
    toggleMessage: this.toggleMessage,
  };

  constructor(props: any) {
    super(props);
    this.toggleMessage = this.toggleMessage.bind(this);
  }

  render() {
    return (
      <JustContext.Provider value={this.state}>
        <GitRepo />
      </JustContext.Provider>
    );
  }
}

export default GitRepoPage;