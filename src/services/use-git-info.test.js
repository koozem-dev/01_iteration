import useGitInfo, {useRequest} from './use-git-info';
import {useCallback} from "react";
import getContent from "./get-content";

const response = {
  data: [
    {
      'id': 33912507,
      'name': '03_iteration',
      'web_url': 'https://gitlab.com/koozem-dev/03_iteration'
    },
    {
      'id': 33554208,
      'name': '01_iteration',
      'web_url': 'https://gitlab.com/koozem-dev/01_iteration',
    },
  ],
  loaded: true,
  error: null
}

jest.mock('./use-git-info');

describe('Use git info', () => {
  it('get git-data', async () => {
      useGitInfo.mockResolvedValue(response);
      const result = await useGitInfo(3849457);
      expect(result).toBe(response);
    }
  )

  it('a nested method have been called', async () => {
      const useRequest = jest.fn();
      useGitInfo.mockImplementation(() => useRequest());
      const result = await useGitInfo(3849457);
      expect(await useRequest).toHaveBeenCalledTimes(1);
    }
  )
})
