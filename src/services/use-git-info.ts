import {useEffect, useMemo, useState, useCallback} from "react";
import {IData} from "../types/types";
import getContent from "./get-content";

interface RequestData {
  data: IData[] | null,
  loaded: boolean,
  error: Error | null
}

type TRequest = () => Promise<any>;

export const useRequest = (request: TRequest): RequestData => {
  // initialState будет пересоздан только при первом рендере компонента
  // Нужно для того, чтоб при изменении state не поменялось значение и не запустился заново рендер
  const initialState = useMemo<RequestData>(() => ({
    data: null,
    loaded: false,
    error: null,
  }), []);

  const [dataState, setDataState] = useState<RequestData>(initialState);

  useEffect((): void | any => {
    setDataState(initialState);
    let cancelled: boolean = false;

    request()
      .then(data => !cancelled && setDataState({
        data,
        loaded: true,
        error: null,
      }))
      .catch(error => !cancelled && setDataState({
        data: null,
        loaded: true,
        error,
      }));
    return () => cancelled = true;
  }, [request, initialState]);

  return dataState;
};

const useGitInfo = (id: number) => {
// Функция getContent будет пересоздана тогда, когда id поменяется.
// Нужно для того, чтоб при изменении state не отправлялся запрос еще раз.
  const request: TRequest = useCallback(() => getContent(id), [id]);
  return useRequest(request);
};

export default useGitInfo;