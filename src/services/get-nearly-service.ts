
export default class GetNearlyService {
  _nearly = [
    {
      id: 1,
      name: 'Пиковая дама',
      date: '11 февраля',
      genre: 'Мистический анекдот',
      author: 'Александр Пушкин',
      value: 0,
    },
    {
      id: 2,
      name: 'Белые розы, розовые слоны',
      date: '12 февраля',
      genre: 'Комедия',
      author: 'Уильям Гибсон',
      value: 0,
    },
    {
      id: 3,
      name: 'Ахов: не все коту масленица',
      date: '13 февраля',
      genre: 'Комедия',
      author: 'Александр Островский',
      value: 0,
    },
  ]

  getItems = async () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (Math.random() > 0.75) {
          reject(new Error('Something bad happened'));
        } else {
          resolve(this._nearly);
        }
      }, 700);
    });
  }
}


