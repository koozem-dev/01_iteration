import axios from "axios";

const getContent = async (groupId: number) => {
  const instance = axios.create({
    baseURL: `https://gitlab.com/api/v4/groups/${groupId}/projects?simple=true`,
  });

  instance.interceptors.request.use(request => {
    request.headers = {'X-SomeText': 'some text'};
    return request;
  });


  return instance.get('')
    .then(response => response.data)
    .catch(error => new Error(`some error: , ${error}`))
}

export default getContent;