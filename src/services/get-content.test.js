
import getContent from './get-content';

jest.mock('./get-content');

describe('Get content', () => {
  let response;
  let error;
  beforeEach(() => {
    response = {
      data: [
        {
          'id': 33912507,
          'name': '03_iteration',
          'web_url': 'https://gitlab.com/koozem-dev/03_iteration',
        },
        {
          'id': 33554208,
          'name': '01_iteration',
          'web_url': 'https://gitlab.com/koozem-dev/01_iteration',
        },
      ],
    };
    error = new Error('some error: ', error);
  });

  it('get data state', async () => {
    getContent.mockReturnValue(response);
    const result = await getContent(3849457);
    expect(result).toEqual(response);
  });

  it('get error', async () => {
    getContent.mockImplementation(error => new Error('some error: ', error));
    const result = await getContent(3849457);
    expect(result).toEqual(error);
  });
})
