import {configureStore} from "@reduxjs/toolkit";
import nearlyReducer from "./reducer";

const store = configureStore({
  reducer: {
    nearly: nearlyReducer
  },
})

export default store;
export type RootState = ReturnType<typeof store.getState>;