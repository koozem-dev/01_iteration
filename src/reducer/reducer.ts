import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from "./store";
import GetNearlyService from "../services/get-nearly-service";
import {IStateNearly} from "../types/types";

const initialState = {
  nearly: [],
  loading: true,
  error: null
} as IStateNearly;

const nearlyService = new GetNearlyService();

export const fetchNearly = createAsyncThunk(
  'nearly/getItems',
  async () => {
      return nearlyService.getItems();
  }
)

export const nearlySlice = createSlice({
  name: 'nearly',
  initialState,
  reducers: {
    increment: (state, action: PayloadAction<number>) => {
      state.nearly.forEach(item => {
        if (item.id === action.payload) {
          item.value += 1;
        }
        return item.value;
      })
    },
    decrement: (state, action: PayloadAction<number>) => {
      state.nearly.forEach(item => {
        if (item.id === action.payload) {
          item.value -= 1;
        }
        return item.value;
      })
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchNearly.fulfilled, (state, action) => {
      if (action.payload instanceof Array) {
        state.nearly = action.payload;
        state.loading = false;
        state.error = null;
      }
    })
    builder.addCase(fetchNearly.rejected, (state, action) => {
      state.nearly = [];
      state.loading = false;
      state.error = action.error;
    })
  },
});

export const {increment, decrement} = nearlySlice.actions;
export const selectNearly = (state: RootState) => state.nearly;
export default nearlySlice.reducer;