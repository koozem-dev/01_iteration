import React, { memo } from 'react';
import JustContext from '../../context/just-context';
import {ISubs} from "../../types/types";

interface SubsProps {
  subs: ISubs[]
}

const SubscriptionsList: React.FC<SubsProps> = ({ subs }) => {
  console.log('SubscriptionsList memoized: i am rendered again?');
  if (!subs) {
    return (<tr>
      <td colSpan={2}>На данный момент абонементов нет</td>
    </tr>);
  }

  return (
    <JustContext.Consumer>
      {
        ({ message }) =>
          subs.map((item) => {
            console.log(message);
            const { number, cost, id } = item;
            return <tr key={id}>
              <td>{number}</td>
              <td>{cost} р.</td>
            </tr>;
          })
      }
    </JustContext.Consumer>
  );
};

export default memo(SubscriptionsList);