import React from 'react';
import Form from './form';
import './form.module.css';

export default {
  title: 'Form',
  component: Form,
  argTypes: {
    bg: {
      options: [ 'transparent', '#f5f5f5', '#333333'],
      control: {
        type: 'select'
      }
    }
  }
};

const Template = args => <Form {...args} />;

export const Primary = Template.bind({});

Primary.args = { bg: 'transparent'};
