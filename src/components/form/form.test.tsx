import React from 'react';
import {render, fireEvent, screen} from '@testing-library/react';
import '@testing-library/jest-dom';

import Form from "./form";
import userEvent from "@testing-library/user-event";

describe('Form', () => {

  it('renders Form component', async () => {
    render(<Form/>);
    screen.debug();
    expect(await screen.findByText(/Имя/i)).toBeInTheDocument();
  })
});

describe('events', () => {

  it('checkbox click', async () => {
    render(<Form/>);
    screen.debug();
    const checkbox = await screen.getByRole('checkbox', {name: /Согласие/i});
    expect(checkbox).toBeChecked();
    fireEvent.click(checkbox);
    expect(checkbox).not.toBeChecked();
  });

  it('throw error', async () => {
    const error = new Error('chooga di pa');
    render(<Form/>);
    const errorButton = await screen.getByRole('button', {name: /Кинуть ошибку/i});
    try {
      userEvent.click(errorButton);
    } catch (e) {
      expect(e).toEqual(error);
    }
  })
})