import {format} from 'date-fns';
import React, {useEffect, useRef, useState} from 'react';
import IMask from "imask";

import UiButton from "../Ui/UiButton/UiButton";
import UiInput from "../Ui/UiInput/UiInput";
import UiTextarea from "../Ui/UiTextarea/UiTextarea";

import styles from './form.module.css';
import styled from "styled-components";

interface Props {
  bg?: 'transparent' | '#f5f5f5' | '#333333'
}

// this is just for practice styled and generic
const StyledForm = styled.div<Props>`
  background-color: ${props => props.bg};
`

const Form: React.FC<Props> = ({bg = 'transparent'}) => {

  const [value, setValue] = useState<string>('');
  const [submitDate, setSubmitDate] = useState<string>(format(new Date(), 'dd.MM.yyyy hh:mm:ss'));
  const [error, setError] = useState<boolean>(false);
  const isInitialMount = useRef<boolean>(true);
  const telephoneRef = useRef<HTMLInputElement>(null);

  useEffect(() => {

    const element = telephoneRef.current;
    IMask(element!, {
      mask: '+{7} (000) 000-00-00',
      lazy: false,  // make placeholder always visible
      placeholderChar: '_'
    });

    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      console.log('я обновился');
    }
  });

  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>): void => {
    setValue(event.target.value);
  };

  if (error) {
    throw new Error('chooga di pa');
  }

  const updateDate = () => {
    setSubmitDate(format(new Date(), 'dd.MM.yyyy kk:mm:ss OOOO'));
  }

  return (
    <StyledForm bg={bg}>
      <form action="https://jsonplaceholder.typicode.com/posts" method="post" className={styles.form} target="_blank">
        <label htmlFor="firstname">Имя*</label>
        <UiInput id="firstname" type="text" name="firstname" placeholder="Василий" required/>

        <label htmlFor="lastname">Фамилия*</label>
        <UiInput id="lastname" type="text" name="lastname" placeholder="Иванов" required/>

        <label htmlFor="telephone">Номер телефона</label>
        <UiInput id="telephone" type="tel" name="telephone" placeholder="+7-900-900-90-90" ref={telephoneRef}/>

        <label htmlFor="email">E-mail*</label>
        <UiInput id="email" type="email" name="email" placeholder="example@gmail.com" required/>

        <label htmlFor="comment">Комментарий</label>
        <UiTextarea id="comment" placeholder="Ваше сообщение" rows={4} cols={50} value={value} onChange={handleChange}/>

        <UiInput id="check" type="checkbox" defaultChecked required className="w15"/>
        <label htmlFor="check"> Согласие на обработку персональных данных </label>

        <label htmlFor="data"></label>
        <UiInput id="date" name="date" type="hidden" value={submitDate} onChange={updateDate}/>

        <UiButton onClick={() => updateDate()} classes="btnSubmit">Отправить</UiButton>
        <UiButton onClick={() => setError(true)}>Кинуть ошибку</UiButton>
      </form>
    </StyledForm>
  );
};

export default Form;
