import React, { Component } from 'react';
import logo from '../../assets/images/logo.png';

class Cat extends Component {
  render() {
    const mouse = this.props.mouse;
    return (
      <img src={logo}
           style={{ position: 'absolute', left: mouse.x, top: mouse.y }}
           alt="" />
    );
  }
}

class Mouse extends Component {
  constructor(props) {
    super(props);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.state = { x: 0, y: 0 };
  }

  handleMouseMove(event) {
    this.setState({
      x: event.clientX,
      y: event.clientY,
    });
  }

  render() {
    return (
      <div style={{ height: '100vh', position: 'relative' }}
           onMouseMove={this.handleMouseMove}>

        {/*
          Вместо статического представления того, что рендерит <Mouse>,
          используем рендер-проп для динамического определения, что надо отрендерить.
        */}
        {this.props.render(this.state)}
      </div>
    );
  }
}

export default class MouseTracker extends Component {
  render() {
    return (
      <div>
        <h1>Перемещайте курсор мыши!</h1>
        <Mouse render={mouse => (
          <Cat mouse={mouse} />
        )} />
      </div>
    );
  }
}