import React, { useState } from 'react';
import logo from '../../assets/images/logo.png';

const Cat = (props) => {

  const mouse = props.mouse;
  return (
    <img src={logo}
         style={{ position: 'absolute', left: mouse.x, top: mouse.y }}
         alt="" />
  );
};

const Mouse = (props) => {

  const [state, setState] = useState({ x: 0, y: 0 });

  const handleMouseMove = (event) => {
    setState({
      x: event.clientX,
      y: event.clientY,
    });
  };

  return (
    <div style={{ height: '100vh', position: 'relative' }}
         onMouseMove={handleMouseMove}>
      {/*
          Вместо статического представления того, что рендерит <Mouse>,
          используем рендер-проп для динамического определения, что надо отрендерить.
        */}
      {props.render(state)}
    </div>
  );

};

const MouseTracker = () => {
  return (
    <div>
      <h1>Перемещайте курсор мыши!</h1>
      <Mouse render={mouse => (
        <Cat mouse={mouse} />
      )} />
    </div>
  );
};

export default MouseTracker;