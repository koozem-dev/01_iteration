import React, { Component } from 'react';

export default class ErrorBoundary extends Component {
  state = { hasError: false};

  constructor(props: any) {
    super(props);
  }

  componentDidCatch() {
    this.setState({
      hasError: true
    });
  }

  render() {
    if (this.state.hasError) {
      return (
        <div> я поймал ошибку </div>
      );
    }
    return this.props.children;
  }
}
