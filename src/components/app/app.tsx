import React, {Suspense, useState} from 'react';
import {Route, Routes} from 'react-router-dom';

import '../../css/normalize.css';
import '../../css/style.css';
import Header from '../header';
import Footer from '../footer';
import Modal from "../modal/modal";

const MainPage = React.lazy(() => import('../../pages/main-page'));
const ContactsPage = React.lazy(() => import('../../pages/contacts-page'));
const SubsPage = React.lazy(() => import('../../pages/subs-page'));
const GitRepoPage = React.lazy(() => import('../../pages/git-repo-page'));

const App = () => {

  const initialState = {
    subs: [
      {number: '4 сеанса', cost: 2050, id: 1},
      {number: '6 сеансов', cost: 3100, id: 2},
      {number: '8 сеансов', cost: 4100, id: 3},
    ]
  }
  const [state] = useState(initialState);
  const [isOpen, setIsOpen] = useState(true);

  return (
    <>
      <Header/>
      <Modal handleClose={() => setIsOpen(false)} isOpen={isOpen}>
        Look at this beauty!
      </Modal>
      <Suspense fallback={<div>Загрузка...</div>}>
        <Routes>
          <Route path="/" element={<MainPage prop={state}/>}/>
          <Route path="/contacts" element={<ContactsPage/>}/>
          <Route path="/charts" element={<SubsPage/>}/>
          <Route path="/repo" element={<GitRepoPage/>}/>
          <Route path="*" element={<h2>Oops! Page not found!</h2>}/>
        </Routes>
      </Suspense>
      <Footer />
    </>
  );
}

export default App;