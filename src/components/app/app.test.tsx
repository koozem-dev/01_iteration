import React, {Suspense, useState} from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';
import {createMemoryHistory} from "history";

import App from "./app";
import JustContext from '../../context/just-context';
import userEvent from "@testing-library/user-event";
import {BrowserRouter, Router} from "react-router-dom";

const initState = {
  message: 'hi',
  toggleMessage: (newMessage: string) => {
  }
}

describe('App', () => {
  it('should render the contact page', async () => {
    render(
      <JustContext.Provider value={initState}>
        <BrowserRouter>
          <App/>
        </BrowserRouter>
      </JustContext.Provider>
    );
    await userEvent.click(screen.getByRole("link", {name: /КОНТАКТЫ/i}));
    expect(await screen.findByText(/Напишите нам/i)).toBeInTheDocument();
  });

  it('should render the home page default', () => {
    render(
      <JustContext.Provider value={initState}>
        <BrowserRouter>
          <App/>
        </BrowserRouter>
      </JustContext.Provider>
    );
    expect(screen.getByText(/АФИША И БИЛЕТЫ/i)).toBeInTheDocument();
  });

  it('should render the error page', async () => {
    const history = createMemoryHistory();
    history.push('/some/bad/route');

    render(
      <JustContext.Provider value={initState}>
        <Router location={history.location} navigator={history}>
          <App/>
        </Router>
      </JustContext.Provider>
    );
    expect(await screen.findByText(/Oops! Page not found!/i)).toBeInTheDocument();
  })
})

