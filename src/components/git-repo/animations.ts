import { keyframes } from "styled-components";

export const zoom = keyframes`
  0% {
    transform: scale(99%);
  } 
  50% {
    transform: scale(102%);
  }
  100% {
    transform: scale(99%);
  }
`