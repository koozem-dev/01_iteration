import styled, { css } from 'styled-components';
import { zoom } from './animations';

interface IButton {
  // Временный атрибут
  $animated?: boolean
}

export const Button = styled.button<IButton>`
  ${({ $animated }) => 
          $animated
    ? css`
      animation: ${zoom} 5s infinite linear;
                  ` 
                  : css`
                    animation: none
                  `
  
    }
`