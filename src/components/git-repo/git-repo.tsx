import React, {useMemo} from 'react';

import useGitInfo from '../../services/use-git-info';
import JustContext from '../../context/just-context';
import UiButton from "../Ui/UiButton/UiButton";
import {IData} from "../../types/types";

import {Button} from "./button";
import styles from './git-repo.module.scss';
import {Column} from "react-table";
import Table from "../table/table";

const GitRepo: React.FC = () => {
  const groupId = 3849457;
  const {data, loaded, error} = useGitInfo(groupId);
  const memoizedData = useMemo(() => data, [data]);
  const memoizedColumns = useMemo<Column<IData>[]>(
    () => [
      {
        Header: 'Project name',
        accessor: 'name' as keyof IData, // accessor is the "key" in the data
        filter: 'fuzzyText',
      },
      {
        Header: 'URL',
        accessor: 'web_url' as keyof IData,
        filter: 'fuzzyText',
      },
      {
        Header: 'Project ID',
        accessor: 'id' as keyof IData,
        filter: 'fuzzyText',
      },
    ], []
  );

  if (error) {
    return (
      <div className={styles.gitRepo}>
        <span>При загрузке произошла ошибка или данных нет</span>
      </div>
    );
  }

  if (!loaded || !data) {
    return <div className={styles.gitRepo}>
      <span>loading...</span>
    </div>;
  }

  return (
    <JustContext.Consumer>
      {
        ({message, toggleMessage}) =>

          <div className={styles.gitRepo}>
            {/*тут анимированная кнопка просто затем, чтоб потестить style-components*/}

            <span>
              <Button $animated>zoom zoom</Button>
              <UiButton onClick={() => toggleMessage('new message')}>new message</UiButton>
              {message}
              <UiButton onClick={() => toggleMessage('just message')}>just message</UiButton>
            </span>
            {data ? <Table columns={memoizedColumns} data={memoizedData!}/> : null}
          </div>
      }
    </JustContext.Consumer>
  );
};

export default GitRepo;

