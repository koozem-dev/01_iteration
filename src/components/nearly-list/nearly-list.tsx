import React, {useLayoutEffect} from 'react';
import { Link } from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {fetchNearly, selectNearly} from '../../reducer/reducer';

import NearlyItem from '../nearly-item';

import styles from './nearly-list.module.css';
import {Item} from "../../types/types";

const NearlyList = () => {
  const nearlyState  = useSelector(selectNearly);
  const { nearly, error, loading } = nearlyState;
  const dispatch = useDispatch();

  useLayoutEffect(() => {
    dispatch(fetchNearly())
  }, [dispatch])

  if (loading) {
    return <div>loading...</div>;
  }

  if (error) {
    return <div>
      <p>Ooooops! Something has gone terribly wrong</p>
      <p>Please, try to reload this page</p>
    </div>;
  }

  const elements = nearly.map((item: Item) => {
    const { ...itemProps } = item;
    return (<NearlyItem key={item.id} {...itemProps} />);
  });

  return (<section className={`${styles.nearly} clearfix`}>
    <div className={styles.content}>
      <h1 className={styles.mainHeader}>Ближайшие спектакли</h1>
      <hr style={{ width: '30%' }} />
      <span><Link to="/">Афиша</Link></span>
      <hr style={{ width: '30%' }} />

    </div>

    {elements}
  </section>);
};

export default NearlyList;
