import React from 'react';
import {render, fireEvent, screen, waitFor, getByRole} from '@testing-library/react';
import '@testing-library/jest-dom';

import NearlyList from "./index";
import store from "../../reducer/store";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import userEvent from "@testing-library/user-event";

const initState = {
  message: 'hi',
  toggleMessage: (newMessage: string) => {
  }
}

describe('NearlyList', () => {

  it('renders NearlyItem component and events', async () => {
    render( <BrowserRouter><Provider store={store}><NearlyList/> </Provider></BrowserRouter>) ;
    try {
      expect(await screen.findByText(/Ooooops/)).toBeInTheDocument();
    } catch (e) {
      expect(await screen.findByText(/Ближайшие спектакли/i)).toBeInTheDocument();

      const buttons = screen.getAllByRole('button', {name: /купить билет/i});
      userEvent.click(buttons[0]);
      expect(await screen.findByText(/^1$/)).toBeInTheDocument();

      userEvent.click(screen.getByRole('button', {name: '+'}));
      expect(await screen.findByText(/^2$/)).toBeInTheDocument();

      userEvent.click(screen.getByRole('button', {name: '-'}));
      userEvent.click(screen.getByRole('button', {name: '-'}));
      expect(await screen.queryByRole('button', {name: '-'})).not.toBeInTheDocument();
    }
  })
});