import {FilterProps, FilterValue, IdType, Row, useAsyncDebounce} from "react-table";
import React from "react";
import {matchSorter} from "match-sorter";
import styles from "./table.module.css";

interface IGlobalFilterProps<T extends object> {
  preGlobalFilteredRows: Row<T>[],
  globalFilter: string,
  setGlobalFilter: (filterValue: any) => void
}

export function GlobalFilter<T extends Record<string, unknown>>({
                                                           preGlobalFilteredRows,
                                                           globalFilter,
                                                           setGlobalFilter
                                                         }: IGlobalFilterProps<T>) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = React.useState(globalFilter);

  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined)
  }, 200);

  return (
    <span className={styles.tableGlobalFilter}>
      Search:{' '}
      <input
        value={value || ''}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
        className={styles.tableGlobalFilterInput}
      />
    </span>
  )
}

export function DefaultColumnFilter<T extends Record<string, unknown>>({
                                                                  column: {
                                                                    filterValue,
                                                                    preFilteredRows,
                                                                    setFilter
                                                                  },
                                                                }: FilterProps<T>) {
  const count = preFilteredRows.length;
  const [value, setValue] = React.useState(filterValue || '');
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    setFilter(event.target.value || undefined);
  };

  return (
    <input className={styles.tableDefaultFilter}
      value={value}
      onClick={(e: React.MouseEvent<HTMLInputElement>) => e.stopPropagation()}
      onChange={handleChange}
      placeholder={`Search ${count} records...`}
    />
  )
}

export function fuzzyTextFilterFn<T extends Record<string, unknown>>(
  rows: Row<T>[],
  id: IdType<T>[],
  filterValue: FilterValue
): Row<T>[] {
  return matchSorter(rows, filterValue, {
    keys: [(row: Row<T>) => row.values[id[0]]]
  })
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = (val: any) => !val