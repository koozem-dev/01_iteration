import React, {ReactNode, useLayoutEffect, useState} from "react";
import {createPortal} from 'react-dom';

interface PortalProps {
  children: ReactNode,
  wrapperId: string
}

const ReactPortal: React.FC<PortalProps> = ({ children, wrapperId = "react-portal-wrapper"}) => {
  const [ wrapperElement, setWrapperElement ] = useState<null | HTMLElement>(null);

  useLayoutEffect(() => {
    let element = document.getElementById(wrapperId);
    let systemCreated = false;
    // if element is not found with wrapperId,
    // create and append to body
    if (!element) {
      systemCreated = true;
      element = createWrapperAndAppendToBody(wrapperId);
    }
    setWrapperElement(element);

    return () => {
      // delete the programatically created element
      if (systemCreated && element?.parentNode) {
        element.parentNode.removeChild(element);
      }
    }
  }, [wrapperId])

  if (wrapperElement === null) return null;

  return createPortal(children, document.getElementById(wrapperId)!);
}

export default ReactPortal;

function createWrapperAndAppendToBody(wrapperId: string) {
  const wrapperElement = document.createElement('div');
  wrapperElement.setAttribute("id", wrapperId);
  document.body.appendChild(wrapperElement);
  return wrapperElement;
}
