import React from 'react';
import { Link } from 'react-router-dom';

import styles from './header.module.css';

const Header = () => {
  return (
    <header className={`${styles.header} clearfix`}>
      <div className={styles.container}>
        <nav>
          <div className={styles.logo}>
            <Link to="/"> </Link>
          </div>
          <ul className={styles.navList}>
            <li className={styles.navItem}>
              <Link to="/" className={styles.navLink}>АФИША И БИЛЕТЫ</Link>
            </li>
            <li className={styles.navItem}>
              <Link to="/charts" className={styles.navLink}>АБОНЕМЕНТЫ</Link>
            </li>
            <li className={styles.navItem}>
              <Link to="/contacts" className={styles.navLink}>КОНТАКТЫ</Link>
            </li>
            <li className={styles.navItem}>
              <Link to="/repo" className={styles.navLink}>Git repositories</Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;