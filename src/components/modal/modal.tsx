import React, {useEffect} from "react";
import ReactPortal from "../react-portal/react-portal";
import styles from "./modal.module.css";
import UiButton from "../Ui/UiButton/UiButton";

interface ModalProps {
  children: React.ReactNode,
  isOpen: boolean,
  handleClose: () => void
}

const Modal: React.FC<ModalProps> = ({children, isOpen, handleClose}) => {
  useEffect(() => {
    const closeOnEscapeKey = (e: KeyboardEvent) => e.key === "Escape" ? handleClose() : null;
    document.body.addEventListener("keydown", closeOnEscapeKey);
    return () => {
      document.body.removeEventListener("keydown", closeOnEscapeKey);
    };
  }, [handleClose]);

  if (!isOpen) return null;

  return (
    <ReactPortal wrapperId="react-portal-modal-container">
      <div className={styles.modal}>
        <UiButton onClick={handleClose}>Мило. Закрой, пожалуйста</UiButton>
        <div className={styles.modalContent}></div>
        <div className={styles.modalContentText}>{children}</div>
      </div>
    </ReactPortal>
  );
}
export default Modal;