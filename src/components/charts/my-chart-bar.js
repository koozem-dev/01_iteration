import React from 'react';

import { Chart as ChartJS, registerables } from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(...registerables);

const MyChartBar = () => {
  const barChartData = {
    labels: ['October', 'November', 'December', 'January', 'February'],
    datasets: [
      {
        data: [-5, 50, 20, 0, -3],
        label: 'First data',
        borderColor: '#3f3f3f',
        backgroundColor: 'rgba(50, 50, 50, 0.5)',
        fill: true,
      },
      {
        data: [0, 8, 66, 48, 2],
        label: 'Second data',
        borderColor: '#ff3333',
        backgroundColor: 'rgba(255, 0, 0, 0.5)',
        fill: true,
      },
    ],
  };

  return (
    <Bar
      type="bar"
      width={1500}
      height={350}
      options={{
        plugins: {
          title: {
            display: true,
            text: 'Some data bar',
            fontSize: 30,
          },
          legend: {
            display: true, //Is the legend shown?
            position: 'top', //Position of the legend.
          },
        },
        layout: {
          padding: 20,
        },
      }
      }
      data={barChartData}
    />
  );
};
export default MyChartBar;