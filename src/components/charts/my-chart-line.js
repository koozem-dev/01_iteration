import React from 'react';

import { Chart as ChartJS, registerables } from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(...registerables);

const MyChartLine = () => {
  const lineChartData = { // все данные и стили диаграммы
    labels: ['October', 'November', 'December', 'January', 'February'],
    datasets: [
      {
        data: [-5, 50, 20, 0, -3],
        label: 'First line',
        borderColor: '#3f3f3f',
        fill: true,
        lineTension: 0.3, // кривизна отрезков
      },
      {
        data: [0, 8, 66, 48, 2],
        label: 'Second line',
        borderColor: '#ff3333',
        backgroundColor: 'rgba(255, 0, 0, 0.5)',
        fill: true,
        lineTension: 0.3,
      },
    ],
  };

  return (
    <Line
      type="line"
      width={1500}
      height={400}
      options={{
        plugins: {
          title: {
            display: true,
            text: 'Some data line',
            fontSize: 30,
          },
          legend: {
            display: true, //Is the legend shown?
            position: 'top', //Position of the legend.
          },
        },
        layout: {
          padding: 20,
        },
      }}
      data={lineChartData}
    />
  );
};
export default MyChartLine;