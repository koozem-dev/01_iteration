import React from 'react';

import { Chart as ChartJS, registerables } from 'chart.js';
import { Pie } from 'react-chartjs-2';

ChartJS.register(...registerables);

const MyChartPie = () => {
  const pieChartData = { // все данные и стили диаграммы
    labels: ['October', 'November', 'December', 'January', 'February'],
    datasets: [
      {
        data: [5, 50, 20, 1, 3],
        label: 'First line',
        borderColor: ['#3f3f3f', '#ff3333', '#949494', '#d06060', '#3e2020'],
        borderWidth: 1,
        backgroundColor: ['#3f3f3f', '#ff3333', '#949494', '#d06060', '#3e2020'],
      }],
  };

  return (
    <Pie
      type="pie"
      width={1500}
      height={400}
      options={{
        plugins: {
          title: {
            display: true,
            text: 'Some data pie',
            fontSize: 30,
          },
          legend: {
            display: true, //Is the legend shown?
            position: 'right', //Position of the legend.
          },
        },
        layout: {
          padding: 20,
        },
        responsive: false,
      }}
      data={pieChartData}
    />
  );
};
export default MyChartPie;