import React, { useEffect } from 'react';
import { Chart } from 'chart.js';

const JSChartBar = () => {
  const barChartData = {
    type: 'bar',
    data: {
      labels: ['October', 'November', 'December', 'January', 'February'],
      datasets: [
        {
          data: [-5, 50, 20, 0, -3],
          label: 'First data',
          borderColor: '#3f3f3f',
          borderWidth: 1,
          backgroundColor: 'rgba(50, 50, 50, 0.5)',
          fill: true,
        },
        {
          data: [0, 8, 66, 48, 2],
          label: 'Second data',
          borderColor: '#ff3333',
          borderWidth: 1,
          backgroundColor: 'rgba(255, 0, 0, 0.5)',
          fill: true,
        },
      ],
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'Some data js bar',
          fontSize: 30,
        },
      },
      scales: {
        y: {
          min: -10,
          max: 70,
        },
      },
      layout: {
        padding: 20,
      },
    },
  };

  useEffect(() => {
    const ctx = document.getElementById('myChart');
    const myChart = new Chart(ctx, barChartData);
  });

  return (
    <canvas id="myChart" width={1500} height={400}></canvas>
  );
};
export default JSChartBar;