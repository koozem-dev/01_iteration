import React from 'react';

import NearlyItem from './index';

import { Provider } from 'react-redux';

import { configureStore, createSlice } from '@reduxjs/toolkit';

export const MockedState = {
  id: 1,
  name: 'Пиковая дама',
  date: '11 февраля',
  genre: 'Мистический анекдот',
  author: 'Александр Пушкин',
  value: 0,
};

const MockStore = ({ nearlyState, children }) => (
  <Provider
    store={configureStore({
      reducer: {
        nearly: createSlice({
          name: 'nearly',
          initialState: nearlyState,
          reducers: {
            increment: (state, action) => {
              state.value += 1;
            },
            decrement: (state, action) => {
              state.value -= 1;
            },
          },
        }).reducer,
      },
    })}
  >
    {children}
  </Provider>
);

export default {
  component: NearlyItem,
  title: 'NearlyItem',
  decorators: [(story) => <div style={{ padding: '3rem' }}>{story()}</div>],
  excludeStories: /.*MockedState$/,
};

const Template = (args) => <NearlyItem {...args} />;

export const Default = Template.bind({});

Default.args = {
  ...MockedState
};

Default.decorators = [
  (story) => <MockStore nearlyState={MockedState}>{story()}</MockStore>,
];

export const Choose = Template.bind({});

Choose.args = {
  ...MockedState,
  value: 1
};

Choose.decorators = [
  (story) => <MockStore nearlyState={MockedState}>{story()}</MockStore>,
];

