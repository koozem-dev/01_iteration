import React from 'react';
import {increment, decrement} from '../../reducer/reducer';
import {useDispatch} from 'react-redux';

import styles from './nearly-item.module.css';
import UiButton from "../Ui/UiButton/UiButton";
import {Item} from "../../types/types";

const NearlyItem: React.FC<Item> = ({name, date, genre, author, id, value}) => {

  const dispatch = useDispatch();

  // @ts-ignore
  const className = [styles.content, styles[`content-${id}`]].join(' ');

  let byeButtons = <UiButton classes={'buy'}
                             onClick={() => dispatch(increment(id))} >Купить билет</UiButton>;

  if (value > 0) {
    byeButtons = <>
      <UiButton classes={'buy'}
                onClick={() => dispatch(decrement(id))} >-</UiButton>
      <span>{value}</span>
      <UiButton classes={'buy'}
                onClick={() => dispatch(increment(id))} >+</UiButton>
    </>;
  }

  return (
    <div className={className}>
      <p className={styles.date}>{date}</p>
      <div className={styles.info}>
        <p className={styles.genre}>{genre}</p>
        <h2 className={styles.header}>
          <p className={styles.name}>{name}</p>
        </h2>
        <p className={styles.author}>{author}</p>
        {byeButtons}
      </div>
    </div>
  );

};

export default NearlyItem;