import UiTextarea, {PropInput} from './UiTextarea';
import { Story, Meta } from '@storybook/react/types-6-0';

export default {
  title: 'Ui-Kit/UiTextarea',
  component: UiTextarea,
} as Meta;

const Template: Story<PropInput> = args => <UiTextarea {...args} />;

const props = {
  id: 'comment',
  value: '',
  placeholder: 'type some text',
  className: 'formText',
  rows: 4,
  cols: 50,
  onChange: () => console.log('some changes'),
};

export const Default = Template.bind({});
Default.args = {
  ...props,
};