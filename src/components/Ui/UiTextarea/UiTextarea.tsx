import React from 'react';
import styles from './UiTextarea.module.css';

export interface PropInput {
  id: string,
  value?: string,
  placeholder: string,
  rows: number,
  cols: number,
  onChange?: any,
}

const UiTextarea: React.FC<PropInput> = (props) => {
    return (
      <textarea className={styles.formText} {...props} maxLength={200} ></textarea>
    );
};

export default UiTextarea;