import UiButton, { PropButton } from './UiButton';
import { Story, Meta } from '@storybook/react/types-6-0';

export default {
  title: 'Ui-Kit/UiButton',
  component: UiButton,
} as Meta;

const Template: Story<PropButton> = (args) => <UiButton {...args}>{args.children}</UiButton>;

const props = {
  children: 'Click me',
  onClick: () => console.log('clicked'),
  classes: 'btn',
};

export const Primary = Template.bind({});

Primary.args = {
  ...props,
  classes: 'btn',
};

export const Submit = Template.bind({});

Submit.args = {
  ...props, classes: 'btnSubmit',
};