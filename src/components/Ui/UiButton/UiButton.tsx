import React from 'react';
import styles from './UiButton.module.css';

export interface PropButton {
  onClick?: () => void,
  children: React.ReactNode,
  classes?: 'btnSubmit' | 'buy' | 'displayNone' | 'btn',
  id?: string
}

const UiButton: React.FC<PropButton> = (props) => {
  const {children, classes = 'btn'} = props;

  return (
    <button className={`${styles.btn} ${styles[classes]}`} {...props}>
      {children}
    </button>
  )
}

export default UiButton;