import React, {forwardRef} from 'react';
import styles from './UiInput.module.css';

export interface PropInput {
  id: string,
  type: string,
  name?: string,
  value?: string,
  defaultChecked?: boolean,
  placeholder?: string,
  required?: boolean,
  className?: 'formInput' | 'w15',
  onChange?: any,
  ref?: React.ForwardedRef<HTMLInputElement>
}

const UiInput: React.FC<PropInput> = forwardRef<HTMLInputElement, PropInput>( (props, ref) => {
  const { className = 'formInput' } = props;

  return (
    <input className={styles[className]} {...props} minLength={2} maxLength={100} ref={ref}/>
  );
});

export default UiInput;

UiInput.displayName = 'UiInput';