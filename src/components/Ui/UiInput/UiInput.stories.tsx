import UiInput, {PropInput} from './UiInput';
import { Story, Meta } from '@storybook/react/types-6-0';

export default {
  title: 'Ui-Kit/UiInput',
  component: UiInput,
} as Meta;

const Template: Story<PropInput> = args => <UiInput {...args} />;

const props = {
  id: 'firstname',
  type: 'text',
  name: 'name',
  value: '',
  defaultChecked: true,
  placeholder: 'type some text',
  required: false,
  className: 'formInput',
  onChange: () => console.log('some changes'),
};

export const Default = Template.bind({});
Default.args = {
  ...props,
  className: 'formInput'
};

export const Telephone = Template.bind({});
Telephone.args = {
  ...props,
  className: 'formInput',
  type: 'tel',
  placeholder: '+7-900-900-90-90',
};

export const Email = Template.bind({});
Email.args = {
  ...props,
  className: 'formInput',
  type: 'email',
  placeholder: 'example@gmail.com',
};

export const Checkbox = Template.bind({});
Checkbox.args = {
  ...props,
  type: 'checkbox',
  className: 'w15'
};

export const Hidden = Template.bind({});
Hidden.args = {
  ...props,
  className: 'formInput',
  type: 'hidden',
  value: new Date().toString(),
};